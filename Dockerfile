FROM openjdk:21-ea-17-slim-buster

COPY target/*.jar /a/revpay.jar

CMD ["java", "-jar", "/a/revpay.jar"]
