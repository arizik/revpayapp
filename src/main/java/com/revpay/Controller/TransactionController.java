package com.revpay.Controller;

import com.revpay.Exceptions.ServiceException;
import com.revpay.Service.AccountService;
import com.revpay.Service.TransactionService;
import com.revpay.model.Transaction;
import com.revpay.model.TransferRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("*")
@RestController
public class TransactionController {
	
	TransactionService transactionService;
	AccountService accountService;
	
	public TransactionController(TransactionService transactionService, AccountService accountService) {
		this.transactionService = transactionService;
		this.accountService = accountService;
	}
	
	
	/*
	 * As a user I should be able to transfer money
	 * POST localhost:9000/transfer
	 * 
	 * {
	 *  "senderId": 1,
	 *  "receiverId": 2
	 *  "amount": 50
	 * }
	 * 
	 */
	
	@PostMapping("transfer")
	public ResponseEntity<String> transferBalance(@RequestBody TransferRequest transferRequest) {
		
		try {
			transactionService.transferBalance(transferRequest.getSenderId(), transferRequest.getReceiverId(), transferRequest.getAmount());
			return ResponseEntity.ok("Balance transferred successfully!");
		} catch (ServiceException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}
	
	
	/*
	 * See all transactions
	 * GET localhost:9000/transactions
	 */
	
	@GetMapping("transactions")
	public List<Transaction> getAllTransactions() {
		return transactionService.getAllTransactions();
	}
	
	
	/*
	 * Transaction history
	 * Find transactions that have the {id} either as the sender or receiver
	 * GET localhost:9000/transactions/1
	 */
	
	@GetMapping("transactions/{id}")
	public List<Transaction> getAllTransactionsById(@PathVariable long id) {
		return transactionService.getAllTransactionsById(id);
	}
	
	
	@GetMapping("transactions/pending/{id}")
	public List<Transaction> getAllPendingTransactionsById(@PathVariable long id) {
		return transactionService.getAllPendingTransactionsById(id);
	}
	
	
	@GetMapping("transactions/requests/{id}")
	public List<Transaction> getAllRequestsById(@PathVariable long id) {
		return transactionService.getAllRequestsById(id);
	}
	
	
	/*
	 * As a user I should be able to request money
	 * POST localhost:9000/request
	 * 
	 * {
	 *  "senderId": 1,
	 *  "receiverId": 2
	 *  "amount": 50
	 * }
	 */
	
	@PostMapping("request")
	public ResponseEntity<String> requestBalance(@RequestBody TransferRequest transferRequest) {
		
		try {
			transactionService.requestBalance(transferRequest.getSenderId(), transferRequest.getReceiverId(), transferRequest.getAmount());
			return ResponseEntity.ok("Request sent successfully. Once user approves, amount will be transferred.");
		} catch (ServiceException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}
	
	
	/*
	 * As a user I should be able to accept the request 
	 * POST localhost:9000/accept-request
	 */
	
	@PostMapping("accept-request/{transactionId}")
	public ResponseEntity<String> acceptRequest(@PathVariable long transactionId) {
		
		try {
			transactionService.acceptRequest(transactionId);
			return ResponseEntity.ok("Request accepted. Balance transferred successfully!");
		} catch (ServiceException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}
	
	
	
	/*
	 * As a user I should be able to deny the request 
	 * POST localhost:9000/deny-request
	 */
	
	@PostMapping("deny-request/{transactionId}")
	public ResponseEntity<String> denyRequest(@PathVariable long transactionId) {
		
		try {
			transactionService.denyRequest(transactionId);
			return ResponseEntity.ok("Request denied.");
		} catch (ServiceException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}
}

