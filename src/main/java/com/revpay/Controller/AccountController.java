package com.revpay.Controller;

import java.util.List;
import java.util.Optional;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.revpay.Exceptions.ServiceException;
import com.revpay.Service.AccountService;
import com.revpay.model.Account;

@CrossOrigin("*")
@RestController
public class AccountController {
	
	AccountService accountService;
	
	public AccountController(AccountService accountService) {
		this.accountService = accountService;
	}
	
	/*
	 * As a user I should be able to register
	 * POST localhost:9000/register
	 */
	
	@PostMapping("register")
	public Account register(@RequestBody Account account) throws ServiceException{
		String hashed = BCrypt.hashpw(account.getPassword(), BCrypt.gensalt(12));
		account.setPassword(hashed);
		
		return accountService.register(account);
	}
	
	
	/*
	 * As a user I should be able to login
	 * POST localhost:9000/login
	 */
	
	@PostMapping("login")
	public Account login(@RequestBody Account account) throws ServiceException{
		return accountService.login(account);
	}


	/*
	 * find people by id
	 * GET localhost:9000/search/id/{id}
	 */

	@GetMapping("search/id/{id}")
	public Optional<Account> findAccountById(@PathVariable long id){
		return accountService.findAccountById(id);
	}


	/*
	 * As a user I should be able to find people by username
	 * GET localhost:9000/search/username/{username}
	 * You don't have to type the complete username
	 */

	@GetMapping("search/username/{username}")
	public List<Account> findAccountByUsername(@PathVariable String username){
		return accountService.findAccountByUsername(username);
	}
	
	
	/*
	 * As a user I should be able to find people by email
	 * GET localhost:9000/search/email/{email} 
	 * You don't have to type the complete email
	 */
	 
	@GetMapping("search/email/{email}")
	public List<Account> findAccountByEmail(@PathVariable String email){
		return accountService.findAccountByEmail(email);
	}
	
	
	/*
	 * As a user I should be able to find people by phone
	 * GET localhost:9000/search/phone/{phone} 
	 * You don't have to type the complete phone number
	 */
	 
	@GetMapping("search/phone/{phone}")
	public List<Account> findAccountByPhone(@PathVariable String phone){
		return accountService.findAccountByPhone(phone);
	}
	
	
	/*
	 * See all accounts
	 * GET localhost:9000/accounts
	 */
	
	@GetMapping("accounts")
	public List<Account> getAllAccounts() {
		return accountService.getAllAccounts();
	}
	
}
