package com.revpay.Service;

import com.revpay.Exceptions.ServiceException;
import com.revpay.Repository.AccountRepository;
import com.revpay.Repository.TransactionRepository;
import com.revpay.model.Account;
import com.revpay.model.Message;
import com.revpay.model.Transaction;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class TransactionService {

	AccountRepository accountRepository;
	TransactionRepository transactionRepository;
	
	public TransactionService(AccountRepository accountRepository, TransactionRepository transactionRepository) {
		this.accountRepository = accountRepository;
		this.transactionRepository = transactionRepository;
	}
	
	/*
	 * Transfer amount
	 */
	
	@Transactional
	public void transferBalance(long senderId, long receiverId, double amount) throws ServiceException{
		
		Account senderAccount = accountRepository.findById(senderId)
				.orElseThrow( () -> new ServiceException("Sender account not found"));
		
		Account receiverAccount = accountRepository.findById(receiverId)
				.orElseThrow( () -> new ServiceException("Receiver account not found"));
		
		if (senderAccount.getBalance() < amount) {
			throw new ServiceException("Insufficient balance in sender account");
		}
		
		double senderNewBalance = senderAccount.getBalance() - amount;
		double receiverNewBalance = receiverAccount.getBalance() + amount;
		
		senderAccount.setBalance(senderNewBalance);
		receiverAccount.setBalance(receiverNewBalance);
		
		Transaction transaction = new Transaction();
		transaction.setAmount(amount);
		transaction.setTimestamp(LocalDateTime.now());
		transaction.setSender(senderAccount);
		transaction.setReceiver(receiverAccount);
		transaction.setPending(false);
		transaction.setTransactionMessage("Balance transferred successfully!");

		transactionRepository.save(transaction);

		Message senderNewMessage = new Message();
		senderNewMessage.setContent("You have transferred " + amount + " to user " + receiverId);
		senderNewMessage.setTimestamp(LocalDateTime.now());

		Message receiverNewMessage = new Message();
		receiverNewMessage.setContent("User " + senderId + " has sent you " + amount);
		receiverNewMessage.setTimestamp(LocalDateTime.now());

		senderAccount.getMessages().add(senderNewMessage);
		receiverAccount.getMessages().add(receiverNewMessage);
		
		accountRepository.save(senderAccount);
		accountRepository.save(receiverAccount);
	}
	
	
	/*
	 * Make a money request
	 */
	
	@Transactional
	public void requestBalance(long senderId, long receiverId, double amount) throws ServiceException{
		
		Account senderAccount = accountRepository.findById(senderId)
				.orElseThrow( () -> new ServiceException("Sender account not found"));
		
		Account receiverAccount = accountRepository.findById(receiverId)
				.orElseThrow( () -> new ServiceException("Receiver account not found"));
		
		
		Transaction transaction = new Transaction();
		transaction.setAmount(amount);
		transaction.setTimestamp(LocalDateTime.now());
		transaction.setSender(senderAccount);
		transaction.setReceiver(receiverAccount);
		transaction.setPending(true);
		transaction.setTransactionMessage("Waiting to be approved");
		
		transactionRepository.save(transaction);
		
		Message senderNewMessage = new Message();
		senderNewMessage.setContent("Waiting for your request you sent to user " + receiverId + " to be approved for transaction " + transaction.getId() + " for the amount of " + amount);
		senderNewMessage.setTimestamp(LocalDateTime.now());
		
		Message receiverNewMessage = new Message();
		receiverNewMessage.setContent("User " + senderId + " has sent you a request on transaction " + transaction.getId() + " for the amount of " + amount + ". Please approve or deny.");
		receiverNewMessage.setTimestamp(LocalDateTime.now());
		
		senderAccount.getMessages().add(senderNewMessage);
		receiverAccount.getMessages().add(receiverNewMessage);
		
		accountRepository.save(senderAccount);
		accountRepository.save(receiverAccount);
	}
	
	
	/*
	 * Get all transactions
	 */
	
	public List<Transaction> getAllTransactions() {
		return transactionRepository.findAll();
	}
	
	
	/*
	 * Get all transactions by Id
	 */
	
	public List<Transaction> getAllTransactionsById(long id) {
		return transactionRepository.findTransactionsByAccountId(id);
	}
	
	public List<Transaction> getAllPendingTransactionsById(long id) {
		return transactionRepository.findTransactionsByAccountIdPendingIsTrue(id);
	}
	
	public List<Transaction> getAllRequestsById(long id) {
		return transactionRepository.findRequestsByAccountIdPendingIsTrue(id);
	}
	
	
	@Transactional
	public void acceptRequest(long transactionId) throws ServiceException {
		
		Transaction transaction = transactionRepository.findById(transactionId)
				.orElseThrow(() -> new ServiceException("Transaction not found"));
				
		if (!transaction.isPending()) {
			throw new ServiceException("Transaction has already been processed");
		}
		
		Account receiverAccount = transaction.getReceiver();
		Account senderAccount = transaction.getSender();
		double amount = transaction.getAmount();
		
		if (receiverAccount.getBalance() < amount) {
			throw new ServiceException("Insufficient balance in the account");
			
		}
		
		double senderNewBalance = senderAccount.getBalance() + amount;
		double receiverNewBalance = receiverAccount.getBalance() - amount;
		
		senderAccount.setBalance(senderNewBalance);
		receiverAccount.setBalance(receiverNewBalance);
		
		transaction.setPending(false);
		transaction.setTransactionMessage("Balance transferred successfully!");
		
		Message senderNewMessage = new Message();
		senderNewMessage.setContent("Your request was approved for transaction " + transaction.getId() + " for the amount of " + amount);
		senderNewMessage.setTimestamp(LocalDateTime.now());
		
		Message receiverNewMessage = new Message();
		receiverNewMessage.setContent("You have approved the request for transaction " + transaction.getId() + " for the amount of " + amount);
		receiverNewMessage.setTimestamp(LocalDateTime.now());
		
		senderAccount.getMessages().add(senderNewMessage);
		receiverAccount.getMessages().add(receiverNewMessage);
		
		accountRepository.save(senderAccount);
		accountRepository.save(receiverAccount);
		transactionRepository.save(transaction);
	}
	
	@Transactional
	public void denyRequest(long transactionId) throws ServiceException {
		
		Transaction transaction = transactionRepository.findById(transactionId)
				.orElseThrow(() -> new ServiceException("Transaction not found"));
		
		if (!transaction.isPending()) {
			throw new ServiceException("Transaction has already been processed");
		}
		
		transaction.setPending(false);
		transaction.setTransactionMessage("Transaction denied");
		
		transactionRepository.save(transaction);
	}
	
}
