package com.revpay.Service;

import java.util.List;
import java.util.Optional;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.stereotype.Service;
import com.revpay.Exceptions.ServiceException;
import com.revpay.Repository.AccountRepository;
import com.revpay.model.Account;

@Service
public class AccountService {
	
	AccountRepository accountRepository;
	
	public AccountService(AccountRepository accountRepository) {
		this.accountRepository = accountRepository;
	}
	
	/**
	 * Register
	 */
	
	public Account register(Account account) throws ServiceException{
		
		if(accountRepository.findByEmail(account.getEmail()) != null){
			throw new ServiceException("Account already exists");
		}
		
		return accountRepository.save(account);
		
	}
	
	
	/*
	 * Login
	 */
	
	
	public Account login(Account account) throws ServiceException {
		
		Account accountCredentials = accountRepository.findByEmail(account.getEmail());
		
		if(accountCredentials == null) {
			throw new ServiceException("Invalid username or password");
		}
		
		if(BCrypt.checkpw(account.getPassword(), accountCredentials.getPassword())) {
			return accountCredentials;
		} else {
			throw new ServiceException("Invalid username or password");
		}
	}
	
	
	/*
	 * Find account by Id
	 */
	
	public Account getAccountById(long accountId) throws ServiceException {
		return accountRepository.findById(accountId)
				.orElseThrow(() -> new ServiceException("Account not found with ID: " + accountId));
	}
	
	
	/*
	 * Find account by username
	 */
	
	public List<Account> findAccountByUsername(String username) {
		return accountRepository.findAccountByUsername(username);
	}
	
	/*
	 * Find account by email
	 */
	
	public List<Account> findAccountByEmail(String email) {
		return accountRepository.findAccountByEmail(email);
	}
	
	
	/*
	 * Find account by phone number
	 */
	
	public List<Account> findAccountByPhone(String phone) {
		return accountRepository.findAccountByPhone(phone);
	}
	
	public List<Account> getAllAccounts() {
		return accountRepository.findAll();
	}

    public Optional<Account> findAccountById(long id) {
		return accountRepository.findById(id);
    }
}
