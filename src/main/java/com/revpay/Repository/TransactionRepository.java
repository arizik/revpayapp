package com.revpay.Repository;

import com.revpay.model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long>{

	@Query("SELECT t FROM Transaction t WHERE t.sender.id = :id OR t.receiver.id = :id")
    List<Transaction> findTransactionsByAccountId(@Param("id") Long id);
	
	@Query("SELECT t FROM Transaction t WHERE t.receiver.id = :id AND t.pending = true")
    List<Transaction> findTransactionsByAccountIdPendingIsTrue(@Param("id") Long id);
	
	@Query("SELECT t FROM Transaction t WHERE t.sender.id = :id AND t.pending = true")
    List<Transaction> findRequestsByAccountIdPendingIsTrue(@Param("id") Long id);
}
