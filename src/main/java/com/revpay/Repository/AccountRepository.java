package com.revpay.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.revpay.model.Account;

public interface AccountRepository extends JpaRepository<Account, Long> {

	Account findByEmail(String email);
	
	/*
	 * Finds account by username
	 */
	
	@Query("FROM Account WHERE LOWER(username) LIKE %:username%")
	List<Account> findAccountByUsername(@Param("username") String accountUsername);
	
	
	/*
	 * Finds account by email
	 */
	
	@Query("FROM Account WHERE LOWER(email) LIKE %:email%")
	List<Account> findAccountByEmail(@Param("email") String accountEmail);

	
	/*
	 * Finds account by phone number
	 */
	
	@Query("FROM Account WHERE LOWER(phone) LIKE %:phone%")
	List<Account> findAccountByPhone(@Param("phone") String accountPhone);

}
