package com.revpay.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Entity
@Data
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "transactionData")
public class Transaction {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column
	private double amount;
	
	@Column
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime timestamp;
	
	@Column
	private boolean pending = true;
	
	@Column
	private String transactionMessage;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "sender_account_id")
	private Account sender;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "receiver_account_id")
	private Account receiver;
	
}
